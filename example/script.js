var msg_el = null;
		
function mycallback (ui, deltaX, deltaY)
{
	if (!msg_el)
		msg_el = document.getElementById("messagearea");
	
	msg_el.innerHTML = ui.elementName + " " + deltaX.toFixed(4) + " " + deltaY.toFixed(4);
	// console.log (ui.elementName + " " + deltaX.toFixed(4) + " " + deltaY.toFixed(4));
}

function my_init()
{
	// setup the touch control interface
	var ok = initTouchControls();

	if (!ok) {
		var err_el = document.getElementById("errorarea");
		err_el.innerHTML = "no touch device found";
	} else {
		// add our controls
		var ui;
		ui = addTouchControl ("joystick01", "joystick", {uiColor : '#555555', touchColor : '#222222', uiCallback : mycallback, autoCenter : true });
		ui = addTouchControl ("hslider01",  "hslider",  {uiColor : '#555555', touchColor : '#222222', uiCallback : mycallback, autoCenter : true, roundRect : true });

		ui = addTouchControl ("vslider01",  "vslider",  {uiColor : '#555555', touchColor : '#ff0000', uiCallback : mycallback });
		ui = addTouchControl ("vslider02",  "vslider",  {uiColor : '#555555', touchColor : '#33aa33', uiCallback : mycallback });
		ui = addTouchControl ("vslider03",  "vslider",  {uiColor : '#555555', touchColor : '#3333ff', uiCallback : mycallback });

		ui = addTouchControl ("buttonA",    "round",    {uiColor : '#33ff33', touchColor : '#005500', uiCallback : mycallback });
		ui = addTouchControl ("buttonB",    "round",    {uiColor : '#ff3333', touchColor : '#550000', uiCallback : mycallback });
		ui = addTouchControl ("buttonX",    "round",    {uiColor : '#ffbf00', touchColor : '#551f00', uiCallback : mycallback });
		ui = addTouchControl ("buttonY",    "round",    {uiColor : '#3333ff', touchColor : '#000055', uiCallback : mycallback });

		ui = addTouchControl ("button1",    "left",    {uiColor : '#222222', touchColor : '#999999', uiCallback : mycallback });
		ui = addTouchControl ("button2",    "right",   {uiColor : '#222222', touchColor : '#999999', uiCallback : mycallback });
	}
}